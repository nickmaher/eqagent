package agent

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"grofsoft.com/eqagent/idle"

	"grofsoft.com/eqagent/logfile"
)

type Agent struct {
	settings *Settings
	reader   *logfile.Reader
	client   *http.Client
}

func New(configFile string) (*Agent, error) {

	settings, err := readSettings(configFile)
	if err != nil {
		return nil, fmt.Errorf("read settings: %w", err)
	}

	reader, err := logfile.NewReader(settings.LogFile)
	if err != nil {
		return nil, fmt.Errorf("open logfile: %w", err)
	}

	fmt.Printf("Opened %s\n", settings.LogFile)

	return &Agent{
		settings: settings,
		reader:   reader,
		client:   &http.Client{},
	}, nil
}

func (a *Agent) Run() error {
	for {
		line, err := a.reader.ReadLine()
		if err != nil {
			return err
		}

		fmt.Printf("%s\n", line)
		a.handleLine(line)
	}
}

func (a *Agent) handleLine(line string) {
	r := a.findMatchingRule(line, a.settings.Rules)
	if r == nil {
		return
	}

	idleTime := idle.Time()
	if idleTime < a.settings.IdleTime {
		fmt.Printf("Idle time: %v. Not posting notification\n", idleTime)
		return
	}

	fmt.Printf("Idle time %v. Posting notification...", idleTime)
	err := a.postNotification(line)
	if err != nil {
		fmt.Printf(" Error: %s\n\n", err)
	} else {
		fmt.Printf(" Success!\n")
	}
}

func (a *Agent) findMatchingRule(line string, rules []*Rule) *Rule {
	for _, r := range rules {
		if r.Match.MatchString(line) {
			return r
		}
	}
	return nil
}

func (a *Agent) postNotification(text string) error {
	const URL = "https://api.pushover.net/1/messages.json"

	data := url.Values{}
	data.Set("token", a.settings.Token)
	data.Set("user", a.settings.User)
	data.Set("message", text)

	req, err := http.NewRequest(http.MethodPost, URL, strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}

	resp, err := a.client.Do(req)
	if err != nil {
		return err
	}

	_ = resp
	return nil
}
