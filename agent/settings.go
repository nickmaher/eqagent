package agent

import (
	"regexp"
	"time"

	"grofsoft.com/eqagent/config"
)

type Settings struct {
	LogFile  string
	Token    string
	User     string
	Rules    []*Rule
	IdleTime time.Duration
}

type Rule struct {
	Match *regexp.Regexp
	// TODO: Maybe other options, override the notification text, etc.
}

func readSettings(filename string) (*Settings, error) {

	c, err := config.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	s := &Settings{}

	s.LogFile, err = c.String("logFile")
	if err != nil {
		return nil, err
	}

	s.Token, err = c.String("token")
	if err != nil {
		return nil, err
	}

	s.User, err = c.String("user")
	if err != nil {
		return nil, err
	}

	s.IdleTime = time.Second * time.Duration(c.OptInt("idleTime", 0))

	for _, ruleNode := range c.GetElements("rule") {
		matchStr, err := ruleNode.String("match")
		if err != nil {
			return nil, err
		}

		match, err := regexp.Compile(matchStr)
		if err != nil {
			return nil, err
		}

		s.Rules = append(s.Rules, &Rule{
			Match: match,
		})
	}

	return s, nil
}
