package config

import (
	"bytes"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"strconv"
)

type Node struct {
	Name       string
	Body       string
	Attributes map[string]string
	Elements   []*Node
}

func (n *Node) GetElements(name string) []*Node {
	var r []*Node
	for _, e := range n.Elements {
		if e.Name == name {
			r = append(r, e)
		}
	}
	return r
}

func (n *Node) String(k string) (string, error) {
	s, ok := n.Attributes[k]
	if ok {
		return s, nil
	}

	for _, e := range n.Elements {
		if e.Name == k {
			return e.Body, nil
		}
	}

	return s, fmt.Errorf("config key %s not found", k)
}

func (n *Node) OptString(k string, def string) string {
	s, err := n.String(k)
	if err == nil {
		return s
	} else {
		return def
	}
}

func (n *Node) Int(k string) (int, error) {
	s, err := n.String(k)
	if err != nil {
		return 0, err
	}

	return strconv.Atoi(s)
}

func (n *Node) OptInt(k string, def int) int {
	i, err := n.Int(k)
	if err == nil {
		return i
	} else {
		return def
	}
}

func (n *Node) Bool(k string) (bool, error) {
	s, err := n.String(k)
	if err != nil {
		return false, err
	}

	return strconv.ParseBool(s)
}

func (n *Node) OptBool(k string, def bool) bool {
	b, err := n.Bool(k)
	if err == nil {
		return b
	} else {
		return def
	}
}

func Read(r io.Reader) (*Node, error) {
	d := xml.NewDecoder(r)
	for {
		t, err := d.Token()
		if err != nil {
			return nil, err
		}

		if t == nil {
			return nil, errors.New("EOF")
		}

		switch v := t.(type) {
		case xml.StartElement:
			return readElement(d, &v)
		}
	}
}

func ReadFile(filename string) (*Node, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	return Read(bytes.NewReader(data))
}

func readElement(d *xml.Decoder, e *xml.StartElement) (*Node, error) {

	node := &Node{Name: e.Name.Local, Attributes: make(map[string]string)}
	for _, a := range e.Attr {
		node.Attributes[a.Name.Local] = a.Value
	}

	for {
		t, err := d.Token()
		if err != nil {
			return nil, err
		}

		if t == nil {
			return nil, errors.New("EOF")
		}

		switch v := t.(type) {
		case xml.StartElement:
			e, err := readElement(d, &v)
			if err != nil {
				return nil, err
			}
			node.Elements = append(node.Elements, e)

		case xml.EndElement:
			return node, nil

		case xml.CharData:
			node.Body += string(v)
		}
	}
}
