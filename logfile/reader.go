package logfile

import (
	"bytes"
	"io"
	"os"
	"strings"
	"time"
)

const pollTime = time.Millisecond * 200
const chunkSize = 1024

type Reader struct {
	reader io.Reader
	buf    []byte
}

func NewReader(name string) (*Reader, error) {

	// Open the log file for reading

	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}

	// Seek to the end

	_, err = f.Seek(0, 2)
	if err != nil {
		return nil, err
	}

	return &Reader{reader: f}, nil
}

func (r *Reader) ReadLine() (string, error) {
	for {

		// Attempt to read a raw line

		line, err := r.readRawLine()
		if err != nil {
			return "", err
		}

		// Look for the timestamp prefix

		if len(line) == 0 || line[0] != '[' {
			continue
		}

		i := strings.IndexByte(line, ']')
		if i < 0 {
			continue
		}

		// Remove it, trim whitespace, and make sure it's non-empty

		line = strings.TrimSpace(line[i+1:])
		if len(line) == 0 {
			continue
		}

		return line, nil
	}
}

func (r *Reader) readRawLine() (string, error) {

	for {

		// Look for a newline in the current buffer.
		// If we found one, return it.

		i := bytes.IndexByte(r.buf, '\n')
		if i >= 0 {
			line := string(r.buf[:i])
			r.buf = r.buf[i+1:]
			return line, nil
		}

		// Read more data from the underlying file

		chunk, err := r.readChunk()
		if err != nil {
			return "", err
		}

		// Append it and try again

		r.buf = append(r.buf, chunk...)
	}
}

func (r *Reader) readChunk() ([]byte, error) {
	c := make([]byte, chunkSize)

	for {

		// Attempt to read new data from the end of the file.
		// If we hit EOF, poll until data is available.

		n, err := r.reader.Read(c)
		if err == io.EOF {
			time.Sleep(pollTime)
			continue
		}
		if err != nil {
			return nil, err
		}
		return c[:n], nil
	}
}
