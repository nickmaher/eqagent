package main

import (
	"fmt"
	"os"

	"grofsoft.com/eqagent/agent"
)

func main() {

	if len(os.Args) != 2 {
		fmt.Printf("Usage: eqagent <eqagent.xml>\n")
		os.Exit(1)
	}

	a, err := agent.New(os.Args[1])
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		os.Exit(1)
	}

	err = a.Run()
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}
}
